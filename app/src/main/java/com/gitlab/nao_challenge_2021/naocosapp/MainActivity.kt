package com.gitlab.nao_challenge_2021.naocosapp

import android.Manifest
import android.content.pm.PackageManager
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.CompoundButton
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import java.io.DataOutputStream
import java.io.OutputStream
import java.lang.Exception
import java.lang.Math.*
import java.net.Socket
import java.util.concurrent.Executors
import kotlin.concurrent.thread
import kotlin.math.log10

private const val REQUEST_RECORD_AUDIO_PERMISSION = 200
private const val SAMPLE_RATE = 8000 // Hz
private const val AUDIO_CHANNEL = AudioFormat.CHANNEL_IN_MONO
private const val AUDIO_FORMAT = AudioFormat.ENCODING_PCM_16BIT

class MainActivity : AppCompatActivity() {

    private var recPermissions: Array<String> = arrayOf(Manifest.permission.RECORD_AUDIO)
    private var recPermissionAccepted = false
    private var recBufferSize = 0
    private var recorder: AudioRecord? = null
    private lateinit var recBuffer: ShortArray
    private var recPeak = 0
    private var recRMS = 0.0
    private lateinit var rmsDisplay: TextView
    private var isRecording = false
    private lateinit var sogliaVolume: SeekBar
    private lateinit var sogliaDisplay: TextView
    private var resultVolume = 666
    private var bloccaTutto = false
    private lateinit var pulsanteReset: Button




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ActivityCompat.requestPermissions(this, recPermissions, REQUEST_RECORD_AUDIO_PERMISSION)
        setContentView(R.layout.activity_main)
        rmsDisplay = findViewById(R.id.rmsDisplay)
        sogliaDisplay = findViewById(R.id.sogliaVolumeTesto)
        pulsanteReset = findViewById(R.id.resetButton)
        sogliaDisplay.text = "soglia volume: $resultVolume"

        sogliaVolume = findViewById(R.id.seekBarSoglia)
        sogliaVolume.setOnSeekBarChangeListener(object :
                SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                //Log.d("prova", "progresso $progress")
                //val resolution = pow(2.0, 16.0) // Using 16 bits as in AUDIO_FORMAT
                resultVolume = progress.toInt()//-(20 * log10(progress.toDouble())).toInt()
                sogliaDisplay.text = "soglia volume: $resultVolume"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        pulsanteReset.setOnClickListener { bloccaTutto = false }

    }

    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        recPermissionAccepted = if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            grantResults[0] == PackageManager.PERMISSION_GRANTED
        } else {
            false
        }
        if (!recPermissionAccepted) {
            finish()
        }
    }

    fun onRecClicked(view: View) {
        val switch = view as CompoundButton
        if (switch.isChecked) {
            startRecording()
        } else {
            stopRecording()
        }
    }

    private fun startRecording() {
        recBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AUDIO_CHANNEL, AUDIO_FORMAT)
        Log.d("Min. buffer size", "$recBufferSize sample(s)")
        recorder = AudioRecord(MediaRecorder.AudioSource.MIC,
                SAMPLE_RATE, AUDIO_CHANNEL, AUDIO_FORMAT, recBufferSize)
        recBuffer = ShortArray(recBufferSize)
        recorder?.startRecording()
        isRecording = true
        thread(start = true) {
            while (isRecording) {
                val len = recorder?.read(recBuffer, 0, recBuffer.size)
                if (len != null) {
                    calcRMS(recBuffer, len)
                    val dB = recRMS.toInt()
                    //Log.d("Mic.", "BufferSize=$len sample(s), vol=$dB dB, raw=$recRMS, pk=$recPeak")
                    //Log.d("NAO", "Bestemmie $dB $resultVolume")
                    rmsDisplay.post { rmsDisplay.text = "Volume $dB" } //fixme: risolvi problema visualizzazione
                    if (resultVolume < dB) {
                        sendNaoCommand()
                        Log.d("NAO", "Bestemmie")
                        bloccaTutto = true
                    }
                }
            }
        }
    }

    private fun calcRMS(buf: ShortArray, len: Int) {
        var accuAbs = 0.0
        recPeak = 0
        for (i in 0..len-1) {
            val v = abs(buf[i].toInt())
            if (recPeak < v) {
                recPeak = v
            }
            accuAbs += (v * v)
        }
        recRMS = sqrt(accuAbs / len)
    }

    private fun toDecibel(recValue: Double) : Int {
        val resolution = pow(2.0, 16.0) // Using 16 bits as in AUDIO_FORMAT
        val min_dB = 20 * log10(1 / resolution)
        val result = 20 * log10(recValue / resolution)
        return if (result < min_dB) {
            min_dB.toInt()
        } else {
            result.toInt()
        }
    }

    private fun stopRecording() {
        isRecording = false
        recorder?.apply {
            if (recordingState == AudioRecord.RECORDSTATE_RECORDING) {
                stop()
            }
            release()
        }
        recorder = null
    }

    override fun onStop() {
        super.onStop()
        stopRecording()
    }

    fun sendNaoCommand() {
        if (bloccaTutto == true) {
            return
        }
        Executors.newSingleThreadExecutor().execute {
            try {
                val socket = Socket("192.168.181.201", 8080)
                println("Connected")
                with(socket) {
                    println("Connected")
                    outputStream.write("silenzio bimbi".toByteArray())
                    close()
                }
            } catch (e: Exception) {
            }
        }
    }
    /*fun segnaleSoglia() {
        var socket =Socket("192.168.181.201", 8080)
        var inputStreamReader =BufferedReader(InputStreamReader(socket.getInputStream()))
        Log.d("ricevuto", "input ${inputStreamReader.readLine()}")
        var outputStreamReader =PrintWriter(socket.getOutputStream())
        outputStreamReader.println("SHUT THE FUCK UP BITCHES")
        outputStreamReader.flush()
        outputStreamReader.close()
        inputStreamReader.close()
        socket.close()
    }*/
    fun segnaleSoglia() {
        val socket = Socket("192.168.181.201", 8080)
        //println("Connected!")

// get the output stream from the socket.

// get the output stream from the socket.
        val outputStream: OutputStream = socket.getOutputStream()
// create a data output stream from the output stream so we can send data through it
// create a data output stream from the output stream so we can send data through it
        val dataOutputStream = DataOutputStream(outputStream)

        println("Sending string to the ServerSocket")

// write the message we want to send

// write the message we want to send
        dataOutputStream.writeChars("Hello from the other side!")
        dataOutputStream.flush() // send the message

        dataOutputStream.close() // close the output stream when we're done.


        println("Closing socket and terminating program.")
        socket.close()
    }
}
